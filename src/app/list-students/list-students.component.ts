import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.scss']
})
export class ListStudentsComponent implements OnInit {
  liste:Array<{nom: string, prenom: string}>

  constructor() { 
    this.liste = [
      {
        nom: "gaye",
        prenom: "Yatta"
      },
      {
        nom: "Sidibé",
        prenom: "Amy Sokhna"
      },
      {
        nom: "Mbengue",
        prenom: "Ibrahima"
      }
    ]
  }

  ngOnInit() {
  }

}
