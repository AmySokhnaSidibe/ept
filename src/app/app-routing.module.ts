import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { ListStudentsComponent } from './list-students/list-students.component';
import { ListDepartementsComponent } from './list-departements/list-departements.component';

const routes: Routes = [
  {
    path:'',
    component:WelcomeComponent

  },
  {
    path:'listEtudiants',
    component:ListStudentsComponent

  },
  {
    path:'listDepts',
    component:ListDepartementsComponent

  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
